<!DOCTYPE html>
<html lang="en">
    <head>
        <met#aeb3b8et="utf-8">
        <title>CV PHẠM NGỌC SANG</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Free Website Template" name="keywords">
        <meta content="Free Website Template" name="description">

        <!-- Favicon -->
        <link href="cv/img/favicon.ico" rel="icon">

        <!-- Google Web Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@300;400;500;600;700&display=swap" rel="stylesheet">

        <!-- Icon Font Stylesheet -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

        <!-- Libraries Stylesheet -->
        <link href="cv/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="cv/lib/lightbox/css/lightbox.min.css" rel="stylesheet">
        <link href="cv/lib/animate/animate.min.css" rel="stylesheet">

        <!-- Customized Bootstrap Stylesheet -->
        <link href="cv/css/bootstrap.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="cv/css/style.css" rel="stylesheet">
    </head>

    <body>
        <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>

        <div class="container">
            <div class="row g-5">
                <div class="col-lg-4 sticky-lg-top vh-100">
                    <div class="d-flex flex-column h-100 text-center overflow-auto shadow">
                        <img class="w-100 img-fluid mb-4" src="cv/img/avt.jpg" alt="Image">
                        <h1 class="text-primary mt-2">Phạm Ngọc Sang</h1>
                        <div class="mb-4" style="height: 22px;">
                            <h4 class="typed-text-output d-inline-block text-light"></h4>
                            <div class="typed-text d-none">FullStack Web</div>
                        </div>
                        <div class="d-flex justify-content-center mt-auto mb-3">
                            <a class="btn btn-secondary btn-square mx-1" href="#"><i class="fab fa-twitter"></i></a>
                            <a class="btn btn-secondary btn-square mx-1" href="#"><i class="fab fa-facebook-f"></i></a>
                            <a class="btn btn-secondary btn-square mx-1" href="#"><i class="fab fa-linkedin-in"></i></a>
                            <a class="btn btn-secondary btn-square mx-1" href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <!-- About Start -->
                    <section class="py-5 border-bottom wow fadeInUp" data-wow-delay="0.1s">
                        <h1 class="title pb-3 mb-5">Sơ Lược Về Bản Thân</h1>
                        <p>Áp dụng những kinh nghiệm về kỹ năng làm web đã được học và sự hiểu biết về
                            Fullstack. Mong muốn được trở thành một lập trình viên Fullstack để có thể được
                            học hỏi nhiều hơn mà mang giá trị cho khách hàng và có công việc ổn định.</p>
                        <div class="row mb-4">
                            <div class="col-sm-6 py-1">
                                <span class="fw-medium text-primary">Name:</span> Phạm Ngọc Sang
                            </div>
                            <div class="col-sm-6 py-1">
                                <span class="fw-medium text-primary">Birthday:</span> 28/02/2001
                            </div>
                            <div class="col-sm-6 py-1">
                                <span class="fw-medium text-primary">Degree:</span> Thực Tập Sinh
                            </div>
                            <div class="col-sm-6 py-1">
                                <span class="fw-medium text-primary">Experience:</span> Sắp Ra Trường
                            </div>
                            <div class="col-sm-6 py-1">
                                <span class="fw-medium text-primary">Phone:</span> 0798752616
                            </div>
                            <div class="col-sm-6 py-1">
                                <span class="fw-medium text-primary">Email:</span> phamngocsang2802@gmail.com
                            </div>
                            <div class="col-sm-6 py-1">
                                <span class="fw-medium text-primary">Facebook:</span> <a href="https://www.facebook.com/profile.php?id=100029065882593">https://www.facebook.com/profile.php?id=100029065882593</a>
                            </div>
                            <div class="col-sm-6 py-1">
                                <span class="fw-medium text-primary">Address:</span> 8 Hà Văn Tính - Hòa Khánh Nam - Liên Chiểu - Đà Nẵng
                            </div>
                        </div>
                    </section>

                    <section class="py-5 wow fadeInUp" data-wow-delay="0.1s">
                        <h1 class="title pb-3 mb-5">Học Vấn</h1>
                        <div class="border-start border-2 border-light pt-2 ps-5">

                            <div class="position-relative mb-4">
                                <span class="bi bi-arrow-right fs-4 text-light position-absolute" style="top: -5px; left: -50px;"></span>
                                <h5 class="mb-1">Sinh Viên Đại Học Duy Tân</h5>
                                <p class="mb-2">06/2019 - Đến Nay</p>
                            </div>
                            <div class="position-relative mb-4">
                                <span class="bi bi-arrow-right fs-4 text-light position-absolute" style="top: -5px; left: -50px;"></span>
                                <h5 class="mb-1">Khóa Lập Trình FullStack</h5>
                                <p class="mb-2">07/2022 - 11/2022</p>
                            </div>
                        </div>
                    </section>

                    <section class="py-5 border-bottom wow fadeInUp" data-wow-delay="0.1s">
                        <h1 class="title pb-3 mb-5">Kỹ Năng</h1>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="skill mb-4">
                                    <div class="d-flex justify-content-between">
                                        <p class="mb-2">HTML</p>
                                        <p class="mb-2">80%</p>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="skill mb-4">
                                    <div class="d-flex justify-content-between">
                                        <p class="mb-2">CSS</p>
                                        <p class="mb-2">85%</p>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="skill mb-4">
                                    <div class="d-flex justify-content-between">
                                        <p class="mb-2">PHP</p>
                                        <p class="mb-2">90%</p>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="skill mb-4">
                                    <div class="d-flex justify-content-between">
                                        <p class="mb-2">Làm Việc Nhóm</p>
                                        <p class="mb-2">95%</p>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="skill mb-4">
                                    <div class="d-flex justify-content-between">
                                        <p class="mb-2">Vuejs</p>
                                        <p class="mb-2">90%</p>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="skill mb-4">
                                    <div class="d-flex justify-content-between">
                                        <p class="mb-2">Boostrap</p>
                                        <p class="mb-2">85%</p>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="skill mb-4">
                                    <div class="d-flex justify-content-between">
                                        <p class="mb-2">Reactjs</p>
                                        <p class="mb-2">85%</p>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="skill mb-4">
                                    <div class="d-flex justify-content-between">
                                        <p class="mb-2">Git</p>
                                        <p class="mb-2">90%</p>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>

                    <section class="py-5 wow fadeInUp" data-wow-delay="0.1s">
                        <h1 class="title pb-3 mb-5">Kinh Nghiệm</h1>
                        <div class="border-start border-2 border-light pt-2 ps-5">
                            <div class="position-relative mb-4">
                                <span class="bi bi-arrow-right fs-4 text-light position-absolute" style="top: -5px; left: -50px;"></span>
                                <h5 class="mb-1">Web bán đồng hồ</h5>
                                <p class="mb-2">FrontEnd | <small>2021 - 2022</small></p>

                            </div>
                            <div class="position-relative mb-4">
                                <span class="bi bi-arrow-right fs-4 text-light position-absolute" style="top: -5px; left: -50px;"></span>
                                <h5 class="mb-1">Web giao đồ ăn</h5>
                                <p class="mb-2">FrontEnd | <small>2022 - 2023</small></p>
                            </div>
                            <div class="position-relative mb-4">
                                <span class="bi bi-arrow-right fs-4 text-light position-absolute" style="top: -5px; left: -50px;"></span>
                                <h5 class="mb-1">Web Điện Thoại</h5>
                                <p class="mb-2">FullStack <small>2023</small></p>
                                <p>Link project:</p>
                                <a href="https://github.com/phamsang2802/Fullstack_web.git">https://github.com/phamsang2802/Fullstack_web.git</a>
                                <button id="demosanpham" class="btn btn-primary">Demo Sản Phẩm</button>
                                <button id="demoloaisanpham" class="btn btn-primary">Demo Loại Sản Phẩm</button>
                            </div>
                        </div>
                    </section>

                    <section class="py-5 wow fadeInUp" data-wow-delay="0.1s">
                        <h1 class="title pb-3 mb-5">Hoạt Động</h1>
                        <div class="border-start border-2 border-light pt-2 ps-5">

                            <div class="position-relative mb-4">
                                <span class="bi bi-arrow-right fs-4 text-light position-absolute" style="top: -5px; left: -50px;"></span>
                                <h5 class="mb-1">Tình Nguyện Viên</h5>
                                <p class="mb-2">Nhóm tình nguyện Nắng Đại
                                    học Duy tân <small>2019-2021</small></p>
                            </div>
                        </div>
                    </section>

                    <section class="py-5 wow fadeInUp" data-wow-delay="0.1s">
                        <h1 class="title pb-3 mb-5">Sở Thích</h1>
                        <div class="border-start border-2 border-light pt-2 ps-5">
                            <div class="position-relative mb-4">
                                <span class="bi bi-arrow-right fs-4 text-light position-absolute" style="top: -5px; left: -50px;"></span>
                                <h5 class="mb-1">Đọc sách:</h5>
                                <p class="mb-2">Mỗi tháng đọc 1 quyển sách nhiều thể loại</p>
                            </div>
                            <div class="position-relative mb-4">
                                <span class="bi bi-arrow-right fs-4 text-light position-absolute" style="top: -5px; left: -50px;"></span>
                                <h5 class="mb-1">Đá bóng:</h5>
                                <p class="mb-2">Tham gia hoạt động đá bóng của trường
                                    hàng tuần</p>
                            </div>
                        </div>
                    </section>




                    <section class="wow fadeIn" data-wow-delay="0.1s">
                        <div class="bg-secondary text-light text-center p-5">
                            <div class="d-flex justify-content-center mb-4">
                                <a class="btn btn-dark btn-square mx-1" href="#"><i class="fab fa-twitter"></i></a>
                                <a class="btn btn-dark btn-square mx-1" href="#"><i class="fab fa-facebook-f"></i></a>
                                <a class="btn btn-dark btn-square mx-1" href="#"><i class="fab fa-linkedin-in"></i></a>
                                <a class="btn btn-dark btn-square mx-1" href="#"><i class="fab fa-instagram"></i></a>
                            </div>

							<p class="m-0">Mong Nhận Được Phản Hồi Sớm Từ Anh Chị</p>
                        </div>
                    </section>
                </div>
            </div>
        </div>


        <!-- Back to Top -->
        <a href="#" class="back-to-top"><i class="fa fa-angle-double-up"></i></a>

        <!-- JavaScript Libraries -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <script src="cv/lib/wow/wow.min.js"></script>
        <script src="cv/lib/typed/typed.min.js"></script>
        <script src="cv/lib/easing/easing.min.js"></script>
        <script src="cv/lib/waypoints/waypoints.min.js"></script>
        <script src="cv/lib/counterup/counterup.min.js"></script>
        <script src="cv/lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="cv/lib/isotope/isotope.pkgd.min.js"></script>
        <script src="cv/lib/lightbox/js/lightbox.min.js"></script>

        <script src="mail/jqBootstrapValidation.min.js"></script>
        <script src="mail/contact.js"></script>

        <script src="cv/js/main.js"></script>
    </body>
</html>
